<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class TodoTest extends TestCase
{
    /** @test */
    public function user_needs_to_be_loggedin_to_post_todo()
    {
        $response = $this->post('/todo/add', [
            'title' => 'gibberish'
        ]);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function user_can_add_todo()
    {
        $this->be(\App\User::first());
        $response = $this->followingRedirects()
            ->post('/todo/add', [
            'title' => 'Wash the dishes',
        ]);

        $response->assertOK();
    }

    /** @test */
    public function a_title_is_required()
    {
        $this->be(\App\User::first());
        $response = $this->post('/todo/add', [
            'title' => '',
        ]);
        $response->assertSessionHasErrors('title');
    }

    /** @test */
    public function todo_can_be_deleted()
    {
        $user = \App\User::first();
        $this->be($user);
        $todo = \App\Todo::where(['user_id' => $user->id])->inRandomOrder()->first();
        $response = $this->followingRedirects()
            ->post('/todo/delete', [
            'id' => $todo->id,
        ]);
        $response->assertOK();
    }
}
