<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('landing');
})->name('landing');

Auth::routes();

Route::group(['middleware' => 'auth', 'prefix' => 'todo', 'as' => 'todo.'], function() {
    Route::get('/add', 'TodoController@create')->name('create_form');
    Route::post('/add', 'TodoController@store')->name('store');
    Route::get('/edit/{todo}', 'TodoController@edit')->name('edit_form');
    Route::post('/edit', 'TodoController@update')->name('edit');
    Route::post('/delete', 'TodoController@destroy')->name('delete');
});
