<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'provision',
            'email' => 'provision@local.test',
            'password' => bcrypt('password')
        ]);
    }
}
