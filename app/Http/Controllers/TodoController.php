<?php

namespace App\Http\Controllers;

use App\Todo;
use App\Http\Requests\StoreTodo;
use App\Http\Requests\UpdateTodo;
use Illuminate\Support\Facades\Mail;
use App\Mail\TodoCreated;

class TodoController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('todos.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTodo $request)
    {
        $todo = \App\Todo::create([
            'title' => $request->get('title'),
            'user_id' => $request->user()->id
            ]);
        Mail::to($request->user()->email)->send(new TodoCreated($todo, $request->user()));
        return redirect()->route('landing');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Todo  $todo
     * @return \Illuminate\View\View
     */
    public function edit(Todo $todo)
    {
        return view('todos.edit', compact('todo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateTodo $request
     * @return \Illuminate\Support\Facades\Redirect
     */
    public function update(UpdateTodo $request)
    {
        $todo = $request->user()->todo()->where(['id' => $request->id])->first();
        if ($todo->title !== $request->title) {
            $todo->title = $request->title;
            $todo->save();
        }
        return redirect()->route('landing');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Support\Facades\Redirect
     */
    public function destroy()
    {
        $item = request()->user()->todo()->where(['id' => request()->id])->first();
        if ($item) {
            $item->delete();
        }
        return redirect()->route('landing');
    }
}
