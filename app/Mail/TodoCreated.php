<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Todo;
use App\User;

class TodoCreated extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $todo;
    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Todo $todo, User $user)
    {
        $this->todo = $todo;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        sleep(20);
        return $this->view('emails.todo_created');
    }
}
