@extends('layouts.app')

@section('content')
        <div class="d-flex flex-column container">
            @auth
                @include('todos.list')
            @else
                <div class="content">
                    <div class="title m-b-md">
                        TODO list made easy<br />
                        <a href="{{ route('login') }}" style="padding: 0px;">Login</a> to see your TODO list
                    </div>
                </div>
            @endauth
        </div>
@endsection
