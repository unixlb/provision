<div class="d-flex justify-content-center mb-5">
    <a href="{{ route('todo.create_form') }}" class="btn btn-primary">Add TODO</a>
</div>
<div class="d-flex flex-column justify-content-center">
    @forelse (Auth::user()->todo as $item)
        <div class="d-flex flex-column mb-5">
            <div class="row">
                <h2>{{ $item->title }}</h2>
            </div>
            <div class="row font-italic">
                Added: {{ $item->created_at }}
            </div>
            <div class="d-flex mt-2">
                <form method="POST" action="{{ route('todo.delete') }}">
                    <input type="submit" class="btn btn-danger mr-2" value="DELETE" />
                    <input type="hidden" name="id" value="{{ $item->id }}" />
                    @csrf
                </form>
                <a href="{{ route('todo.edit_form', $item->id) }}" class="btn btn-warning">Edit</a>
            </div>
        </div>
    @empty
        Any TODOs you add will appear here
    @endforelse
</div>
