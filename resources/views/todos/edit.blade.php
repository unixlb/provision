@extends('layouts.app')

@section('content')
<div class="d-flex justify-content-center container">
    <form method="POST" action="{{ route('todo.edit') }}">
        <div class="form-group">
            <input type="text" placeholder="TODO" autocomplete="off" class="form-control" name="title" value="{{ $todo->title }}" />
            @error('title')
                {{ $message }}
            @enderror
            @error('id')
                {{ $message }}
            @enderror
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-success" />
            <input type="hidden" name="id" value="{{ $todo->id }}" />
            @csrf
        </div>
    </form>
</div>
@endsection
