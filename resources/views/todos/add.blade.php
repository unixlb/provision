@extends('layouts.app')

@section('content')
<div class="d-flex justify-content-center container">
    <form method="POST" action="{{ route('todo.store') }}">
        <div class="form-group">
            <input type="text" placeholder="TODO" autocomplete="off" class="form-control" name="title" />
            @error('title')
                {{ $message }}
            @enderror
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-success" />
            @csrf
        </div>
    </form>
</div>
@endsection
